# coding=utf-8
# COPYRIGHT (C) 2016 - Jocelyn LOPEZ - jocelyn.lopez.pro@gmail.com

cata_msg = {

    1 : _(u"""
Les paramètres fournis rendent la recherche non unique.
Attention, dans ce cas on renvoie la plus grande valeur des valeurs obtenues.

Jeux de paramètres fournis : %(k1)s
"""),

    2 : _(u"""
Aucune ligne de la table ne correspond aux paramètres fournis.

Jeux de paramètres fournis : %(k1)s

Conseil : Vérifier qu'il existe bien une seule et unique ligne dans la table,
correspondant à votre recherche.
"""),

}
