# coding=utf-8
# COPYRIGHT (C) 2016 - Jocelyn LOPEZ - jocelyn.lopez.pro@gmail.com
import numpy as np
from Utilitai.Utmess import UTMESS


def get_cleaned_dict(TABLE):
    cleaned_TAB = TABLE.EXTR_TABLE().values()
    for key, value in cleaned_TAB.items():
        for i, v in enumerate(value):
            if isinstance(v, str):
                cleaned_TAB[key][i] = v.strip()
    return dict(cleaned_TAB)


def get_value(cleaned_TAB, **args):
    indices = None
    for key, value in args.items():
        index_courant = np.where(np.array(cleaned_TAB[key]) == value)
        if indices is None:
            indices = index_courant
        else:
            indices = np.intersect1d(indices, index_courant)

    if len(indices) > 1:
        UTMESS('A', 'IMPR_JSON_1', valk=str(args))
        values = []
        for index in indices:
            values.append(np.array(cleaned_TAB['VALE'])[np.array([index])][0])
        value = max(values)
        return value
    elif len(indices) == 0:
        UTMESS('A', 'IMPR_JSON_2', valk=str(args))
    else:
        value = np.array(cleaned_TAB['VALE'])[indices][0]
        return value


def impr_json_ops(self, VALEURS, **args):
    """Ecriture de la macro IMPR_JSON."""
    ier = 0

    # La macro compte pour 1 dans la numerotation des commandes
    self.set_icmd(1)

    #----------------------------------------------
    # Fichier
    try:
        unite = args['UNITE']
    except:
        unite = 10

    # Creation du dictionnaire json
    dict_json = {}
    for var_content_FACT in VALEURS:
        var_content = var_content_FACT.cree_dict_toutes_valeurs()
        var_name = var_content['NOM_VALEUR']
        cleaned_TAB = get_cleaned_dict(var_content['TABLE'])
        l_var_filter = var_content['FILTRE']

        # Construction du filtre grace � l'ensemble des cl�s/valeurs
        global_filter = {}
        for local_filter_FACT in l_var_filter:
            local_filter = local_filter_FACT.cree_dict_toutes_valeurs()
            name = local_filter.pop('NOM_PARA')
            valeur = local_filter.values()[0][0]   # Il ne doit rester plus qu'un item dans le dict
            global_filter.update({name: valeur})

        # Recuperation de la valeur pour le filtre fourni
        dict_json[var_name] = get_value(cleaned_TAB, **global_filter)

    # Impression du dictionnaire
    filename = 'fort.' + str(unite)
    if args['FORMAT'] == "JSON":
        import json
        with open(filename, 'w') as ofile:
            json.dump(dict_json, ofile, sort_keys=True, indent=4, ensure_ascii=False)
    elif args['FORMAT'] == "YAML":
        raise NotImplementedError("Impression au format YAML pas encore disponible")
    elif args['FORMAT'] == "XML":
        raise NotImplementedError("Impression au format XML pas encore disponible")

    return ier
